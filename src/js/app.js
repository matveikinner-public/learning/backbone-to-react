define(["backbone"], function (Backbone) {
  class App {
    router;

    initialize() {
      console.log("App initialize");
      Backbone.history.start({ silent: true });
    }
  }

  return new App();
});
