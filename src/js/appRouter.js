// prettier-ignore
define(["backbone", "models/todo/todoItem", "collections/todo/todoItems", "views/todo/todoListView", "views/404/unknownPathView"], function (
  Backbone,
  TodoItem,
  TodoItems,
  TodoListView,
  UnknownPathView
) {
  const AppRouter = Backbone.Router.extend({
    /**
     * Routes property defines records with route and corresponding route handler
     */
    routes: {
      "todos": "todoListViewHandler",
      // "todos/:todoId": "todoDetailsViewHandler",
      "*path": "unknownPathView",
    },

    todoListViewHandler: function () {
      const todoItems = new TodoItems([
        new TodoItem({ description: "Item 1" }),
        new TodoItem({ description: "Item 2" }),
        new TodoItem({ description: "Item 3" }),
      ]);
      const view = new TodoListView({ el: "#root", model: todoItems });
      view.render();
    },

    todoDetailsViewHandler: function (todoId) {
      console.log("todoDetailsView");
    },

    unknownPathView: function () {
      console.log("unknownPathView");
      const view = new UnknownPathView({ el: "#root" });
      view.render();
    },
  });

  return AppRouter;
});
