define(["underscore", "backbone", "models/todo/todoItem"], function (_, Backbone, TodoItem) {
  const TodoItems = Backbone.Collection.extend({
    model: TodoItem,
  });

  return TodoItems;
});
