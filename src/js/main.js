require.config({
  paths: {
    jquery: "lib/jquery-2.2.4/jquery-min",
    underscore: "lib/underscore-1.31.1/underscore-min",
    backbone: "lib/backbone-1.4.0/backbone-min",
    text: "lib/text-2.0.12/text-min",
    templates: "templates",
  },
});

define(["app", "appRouter", "views/nav/navView"], (App, AppRouter, NavView) => {
  $(document).ready(function () {
    const router = new AppRouter();
    App.router = router;

    App.initialize();
    new NavView({ el: "#nav" }).render();
  });
});
