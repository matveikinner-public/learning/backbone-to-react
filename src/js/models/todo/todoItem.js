define(["underscore", "backbone"], function (_, Backbone) {
  var TodoItem = Backbone.Model.extend({
    validate: function (attrs) {
      if (!attrs.description) {
        return "Description is required.";
      }
    },
  });

  return TodoItem;
});
