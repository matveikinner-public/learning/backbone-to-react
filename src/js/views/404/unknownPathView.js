define(["jquery", "underscore", "backbone"], function ($, _, Backbone) {
  const UnknownPathView = Backbone.View.extend({
    render: function () {
      this.$el.html("UNKNOWN PATH 404");
      return this;
    },
  });

  return UnknownPathView;
});
