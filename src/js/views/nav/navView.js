define(["jquery", "underscore", "backbone", "app", "text!templates/navBar.tmpl"], function (
  $,
  _,
  Backbone,
  App,
  template
) {
  const NavView = Backbone.View.extend({
    events: {
      click: "onClick",
    },

    onClick: function (e) {
      const $li = $(e.target);
      App.router.navigate($li.attr("data-url"), { trigger: true });
    },

    render: function () {
      const tmpl = _.template(template);
      this.$el.html(tmpl);
      return this;
    },
  });

  return NavView;
});
