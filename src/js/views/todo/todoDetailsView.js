define(["jquery", "underscore", "backbone", "views/todo/todoItemView"], function ($, _, Backbone, TodoItemView) {
  const TodoDetailsView = Backbone.View.extend({
    id: "todoDetails",

    initialize: function (options) {
      if (!options && options.model) {
        throw new Error("model is not specified");
      }
    },

    render: function () {
      const self = this;

      this.model.each(function (item) {
        const view = new TodoItemView({
          model: item,
        });
        self.$el.append(view.render().$el);
      });
      return this;
    },
  });

  return TodoDetailsView;
});
