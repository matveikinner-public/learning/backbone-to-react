define(["jquery", "underscore", "backbone"], function ($, _, Backbone) {
  const TodoItemView = Backbone.View.extend({
    tagName: "li",

    defaults: {
      description: "",
    },

    initialize: function (options) {
      if (!options && options.model) {
        throw new Error("model is not specified");
      }
    },

    render: function () {
      this.$el.html(this.model.get("description"));
      return this;
    },
  });

  return TodoItemView;
});
