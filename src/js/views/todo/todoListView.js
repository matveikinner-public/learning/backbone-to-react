define(["jquery", "underscore", "backbone", "views/todo/todoItemView"], function ($, _, Backbone, TodoItemView) {
  const TodoListView = Backbone.View.extend({
    tagName: "ul",

    id: "todoHistory",

    initialize: function (options) {
      if (!options && options.model) {
        throw new Error("model is not specified");
      }
    },

    render: function () {
      const self = this;

      this.model.each(function (item) {
        const view = new TodoItemView({
          model: item,
        });
        self.$el.append(view.render().$el);
      });
      return this;
    },
  });

  return TodoListView;
});
